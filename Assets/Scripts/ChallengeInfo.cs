﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChallengeInfo
{
    public Transform BallPlace;
    public Transform PlayerPlace;
    public Transform Gates;
    public Transform CameraPlace;
    public GameObject[] EffectPlaces;
}
