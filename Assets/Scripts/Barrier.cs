﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    public bool staticBarrier = true;

    [Header("Multiple Movement")]
    public bool multiple = false;
    public int movingAmount;
    public int place;

    [Header("Move Attributes")]
    public MoveType moveType;

    public Transform pointer;
    public float speed = 1;
    [Header("Circle")]
    public float radius = 1;


    float timeCounter;

    Vector3 position;

    bool front = false;

    void Start()
    {
        position = transform.position;
        if(moveType == MoveType.line)
        {
            front = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!staticBarrier)
        {
            startMove();
        }
    }

    void startMove()
    {
        if (moveType == MoveType.circle)
        {
            if (multiple)
            {
                timeCounter += speed * Time.deltaTime;
            }
            else
            {
                timeCounter += speed * Time.deltaTime;
            }
            float x = pointer.position.x + radius * Mathf.Cos(timeCounter + Mathf.PI * (place / movingAmount));
            float y = pointer.position.y;
            float z = pointer.position.z + radius * Mathf.Sin(timeCounter + Mathf.PI * (place / movingAmount));

            transform.position = new Vector3(x, y, z);
            return;
        }
        if (moveType == MoveType.line)
        {
            //timeCounter += speed * Time.deltaTime;
            if (front && Vector3.Distance(transform.position, pointer.position) > 0.5)
            {
                Vector3 dir = pointer.position - transform.position;
                transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
                return;
            }
            else
            {
                front = false;
            }
            if(Vector3.Distance(transform.position, position) > 0.5)
            {
                Vector3 dir = position - transform.position;
                transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
                return;
            }
            else
            {
                front = true;
            }
        }
    }
}

public enum MoveType
{
    line,
    circle,
    dance,
}
