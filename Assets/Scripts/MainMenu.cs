﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text Cash;

    public LevelManager levelManager;

    // Start is called before the first frame update
    void Start()
    {
        Cash.text = levelManager.Cash.ToString();
    }
}
