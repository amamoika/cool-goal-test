﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryView : MonoBehaviour
{
    //public float changePercent = 0.04f;

    public GameObject PointPrefab;

    public bool startShow = false;

    ParabolaController parabola;
    //float prevParabolaLength;
    float minimalLength;
    float distance;
    float distanceTime;
    float[] addPointsLengths = new float[2];

    bool timeCalculated = false;

    List<GameObject> points = new List<GameObject>();

    byte minAmount = 16;
    byte curAmmount = 0; 

    void Start()
    {
        parabola = GetComponent<ParabolaController>();
        minimalLength = parabola.parabolaLength;
        //prevParabolaLength = minimalLength;
        countDistance();
    }
    void Update()
    {
        if (startShow)
        {
            if(!timeCalculated)
            {
                countTime();
            }
            //if (parabola.parabolaLength != prevParabolaLength)
            //{
            //    calcDistanceChange(prevParabolaLength);
            //    prevParabolaLength = parabola.parabolaLength;
            //}
            checkPointsLineState();
            updatePointsPositions();
        }
        else
        {
            clear();
        }
    }

    void checkPointsLineState()
    {
        if ((parabola.parabolaLength - (curAmmount * distance)) > distance)
        {
            addPoint((byte)((parabola.parabolaLength - (curAmmount * distance)) / distance));
        }
        else if ((parabola.parabolaLength - (curAmmount * distance)) < 0)
        {
            deletePoint();
        }
    }

    void countDistance()
    {
        distance = minimalLength / minAmount;
    }
    public void countTime()
    {
        distanceTime = parabola.GetDuration() / minAmount;
        timeCalculated = true;
    }

    void addPoint(byte ammount)
    {
        for (byte i = 0; i < ammount; i++)
        {
            points.Add((GameObject)Instantiate(PointPrefab, getPointPosition(curAmmount), Quaternion.identity));
            curAmmount++;
        }
    }
    void deletePoint()
    {
        Destroy(points[curAmmount-1]);
        points.Remove(points[curAmmount-1]);
        curAmmount--;
    }

    Vector3 getPointPosition(byte index)
    {
        return parabola.GetPositionAtTime((index + 1) * distanceTime);
    }
    void updatePointsPositions()
    {
        for (byte i = 0; i < curAmmount; i++)
        {
            points[i].transform.position = getPointPosition(i);
        }
    }

    void clear()
    {
        for (int i = 0; i < points.Count; i++)
        {
            Destroy(points[i]);
        }
        points.Clear();
    }
}
