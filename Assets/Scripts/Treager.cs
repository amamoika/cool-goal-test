﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treager : MonoBehaviour
{
    public Ball ball;

    public Transform rotationTarget;
    
    public float forceMultiplier = 5;

    public bool pulsed = false;

    public float rotationSpeed = 5f;
    float angle = 0f;

    public string groundTag = "Ground";

    public Vector3 nextPoint = new Vector3();

    public Vector3[] parabolaPoints = new Vector3[51];

    private void FixedUpdate()
    {
        //Debug.Log(nextPoint);
        if(ball.Hitted)
        {
            //nextPoint = parabolaPoints[GetComponentInParent<ParabolaController>().GetPointNumber()] - GetComponentInParent<Transform>().localPosition;
            rotationTarget.rotation = Quaternion.Euler(0f, angle, 0f);
            angle += Time.fixedDeltaTime * rotationSpeed;
            transform.LookAt(nextPoint);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!pulsed && ball.Hitted && !other.CompareTag("Flower"))
        {
            ball.parabola.StopFollow();
            ball.parabola.enabled = false;
            throwBall();
            return;
        }
        if(other.CompareTag(groundTag))
        {
            ball.Hitted = false;
        }
    }
    public void throwBall()
    {
        Vector3 dir = new Vector3(nextPoint.x, nextPoint.y - nextPoint.y, nextPoint.z);
        //Vector3 dir = nextPoint - GetComponentInParent<Transform>().transform.position;
        ball.GetComponent<Rigidbody>().AddForce(dir * forceMultiplier, ForceMode.Impulse);
        pulsed = true;
    }
}
