﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserControl : MonoBehaviour
{
    private Camera mainCamera;

    public Vector3 show;

    public float DistanceToGates = 18f;

    public LayerMask groundMask;

    public Transform middlePoint;
    public Transform endPoint;

    public bool Started = false;
    public bool Run = false;

    float nullPosition;
    bool nullPositionSet = false;

    Ball ball;

    bool changingTrajectory = false;
    float flightHeight = 1.5f;

    void Start()
    {
        ball = GetComponent<Ball>();

        mainCamera = Camera.main;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            changingTrajectory = true;
            Started = true;
            GetComponent<ParabolaController>().ParabolaRoot.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 0f);
            middlePoint = GetComponent<ParabolaController>().ParabolaRoot.transform.GetChild(1);
            endPoint = GetComponent<ParabolaController>().ParabolaRoot.transform.GetChild(2);
            GetComponent<TrajectoryView>().enabled = true;
            GetComponent<TrajectoryView>().startShow = true;
        }
        if (changingTrajectory && !Input.GetMouseButtonUp(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f, groundMask))
            {
                if(!nullPositionSet)
                {
                    nullPosition = hit.point.z;
                    nullPositionSet = true;
                }
                if (Mathf.Abs(nullPosition - hit.point.z) <= 3.2)
                {
                    changeTrajectory(hit.point);
                    show = hit.point;
                }
                else
                {
                    changeTrajectory(show);
                }
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            nullPositionSet = false;
            changingTrajectory = false;
            GetComponent<TrajectoryView>().startShow = false;
            this.enabled = false;
            Run = true;
            Invoke("shoot", 0.7f); 
        }
    }
    void changeTrajectory(Vector3 offset)
    {
        changeMiddlePoint((nullPosition - offset.z)*2.5f);
        changeEndPoint((nullPosition - offset.z)*2.5f);
    }
    void changeMiddlePoint(float pos)
    {
        float offset;
        if (pos < 0)
        {
            offset = -3.5f;
        }
        else
        {
            offset = 3.5f;
        }
        if (Mathf.Abs(pos) <= 3.5f)
        {
            offset = pos;
        }
        middlePoint.localPosition = new Vector3((-DistanceToGates) / 2, flightHeight, offset);
    }
    void changeEndPoint(float pos)
    {
        float offset;
        if(pos < 0)
        {
            offset = -3.5f;
        }
        else
        {
            offset = 3.5f;
        }
        if (Mathf.Abs(pos) >= 3.5f)
        {
            offset -= pos - offset;
        }
        else
        {
            offset = pos;
        }
        endPoint.localPosition = new Vector3(-DistanceToGates, 0, offset);
    }
    void shoot()
    {
        ball.Hit();
        Run = false;
    }
}
