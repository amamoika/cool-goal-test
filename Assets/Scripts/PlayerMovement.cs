﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    public LevelManager levelManager;

    public Transform target;

    float speedPercent;

    const float lookMotionAnimationTime = .1f;

    NavMeshAgent agent;
    Animator animator;
    float distance;
    bool ran = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        //speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("SpeedPercent", speedPercent, lookMotionAnimationTime, Time.deltaTime);
        if (ran && Vector3.Distance(transform.position, target.position) < 2f)
        {
            //Hit();
            Invoke("stop", 0.1f);
            //stop();
        }
        //if(Vector3.Distance(transform.position, target.position) < 0.5f)
        //{
        //    agent.isStopped = true;
        //}
    }

    public void Run(Transform _target)
    {
        target = _target;
        speedPercent = 0.5f;
        agent.SetDestination(target.position);
        ran = true;

    }
    void Hit()
    {
        speedPercent = 1;
    }
    void stop()
    {
        speedPercent = 0;
        animator.SetFloat("SpeedPercent", speedPercent, lookMotionAnimationTime, Time.deltaTime*10);
        this.enabled = false;
    }
}
