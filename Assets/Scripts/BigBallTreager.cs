﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBallTreager : MonoBehaviour
{
    // Start is called before the first frame update
    //void OnTriggerEntser(Collider other)
    //{
    //    if(other.CompareTag("Barrier"))
    //    {
    //        other.GetComponent<Rigidbody>().AddForce((other.transform.position - transform.position) * 20, ForceMode.Impulse);
    //        Debug.Log("enter");
    //    }
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Barrier"))
        {
            other.GetComponent<Rigidbody>().AddForce((other.transform.position - transform.position), ForceMode.Impulse);
            Debug.Log("enter");
        }
    }
}
