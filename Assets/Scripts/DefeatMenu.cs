﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class DefeatMenu : MonoBehaviour
{
    public LevelManager levelManager;

    public Text Progress;

    public Button bigButton;
    public Image pointer;
    public Text restartTime;

    public GameObject smallButton;

    float count = 6;
    bool countdown = false;

    private void Start()
    {
        bigButton.enabled = true;
        Progress.text = string.Format("{0}%", (int)((((float)levelManager.CurChallengeNumber) / (float)levelManager.Challenges.Count) * 100));

        if (levelManager.CurChallengeNumber > 1)
        {
            smallButton.SetActive(false);
            pointer.enabled = false;
            bigButton.interactable = false;
            restartTime.text = count.ToString();
            countdown = true;
        }
        else
        {
            pointer.enabled = true;
            smallButton.SetActive(true);
            restartTime.enabled = false;
            bigButton.enabled = true;
        }
    }
    void Update()
    {
        if(countdown)
        {
            count -= Time.deltaTime;
            restartTime.text = ((int)count).ToString();
            if (count <= 3)
            {
                smallButton.SetActive(true);
            }
            else
                return;
            if(count <= 1)
            {
                levelManager.Restart();
            }
        }
    }

    //IEnumerator RestartCountDown()
    //{
        
    //    yield return new WaitForSeconds(1);
    //    i += Time.deltaTime;
    //    Debug.Log("eee");
    //    if(i >= 3)
    //    {
    //        smallButton.enabled = true;
    //    }
    //    if(i >= 6 && !levelManager.restarted)
    //    {
    //        levelManager.Restart();
    //    }
    //}
}
