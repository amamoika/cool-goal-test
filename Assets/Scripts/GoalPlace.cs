﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalPlace : MonoBehaviour
{
    public LevelManager levelManager;
    public bool Goaled = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball") && Goaled == false)
        {
            levelManager.Goal();
            other.GetComponent<Rigidbody>().drag = 7;
            Goaled = true;
        }
    }
}
