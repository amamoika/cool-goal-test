﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [Header("ToGameManager")]
    //put later to rhe GameManager
    public int Cash;

    public short CurChallengeNumber = 0;

    public List<ChallengeInfo> Challenges;

    public float timeBetweenChallenges = 5f;

    public GameObject ballPrefab;
    public GameObject parabolaRootPrefab;
    public GameObject player;

    public bool restarted = false;

    [Header("Effects")]
    public GameObject GoalEffect;

    [Header("LevelInfo")]
    public int reward;
    public int levelNumber;

    [Header("UI")]
    public GameObject mainMenu;
    public GameMenu gameMenu;
    public GameObject defeatUI;
    public GameObject victoryUI;

    private GameObject ballGO;
    private GameObject rootGO;
    private GameObject playerGO;

    private Transform mainCamera;
    private float cameraSpeed = 50f;
    private float cameraRotSpeed;
    private bool cameraTranslating = false;
    private bool cameraRotTime = false;

    private bool goaled = false;
    private bool ran = false;

    void Start()
    {
        mainCamera = Camera.main.transform;
        StartChallenge();
    }

    void Update()
    {
        if (cameraTranslating)
        {
            StartCoroutine(MoveCameraToNewPlace());
        }
        else
        {
            if (ballGO.GetComponent<Ball>().Stoped && !goaled)
            {
                defeatUI.SetActive(true);
            }
            if (ballGO.GetComponent<UserControl>().Started)
            {
                mainMenu.SetActive(false);
            }
            if (ballGO.GetComponent<UserControl>().Run && !ran)
            {
                playerGO.GetComponent<PlayerMovement>().Run(Challenges[CurChallengeNumber].BallPlace);
                ran = true;
            }
        }
    }

    public void Restart()
    {
        if (!restarted)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
        }
    }

    public void NextLevel()
    {
        if (!restarted)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void Goal()
    {
        goaled = true;
        
        if (CurChallengeNumber != Challenges.Count - 1)
        {
            gameMenu.NextChallenge();
            GameObject goalEffect1 = (GameObject)Instantiate(GoalEffect, Challenges[CurChallengeNumber].EffectPlaces[0].transform.position, Quaternion.identity);
            Destroy(goalEffect1, 3f);
            GameObject goalEffect2 = (GameObject)Instantiate(GoalEffect, Challenges[CurChallengeNumber].EffectPlaces[1].transform.position, Quaternion.identity);
            Destroy(goalEffect2, 3f);
            Invoke("NextChallenge", timeBetweenChallenges);
        }
        else
        {
            gameMenu.Fill();
            Invoke("Victory", timeBetweenChallenges);
        }
    }
    void Victory()
    {
        victoryUI.SetActive(true);
    }
    public void Defeat()
    {
        restarted = false;
        defeatUI.SetActive(true);
    }
    void NextChallenge()
    {
        CurChallengeNumber++;
        cameraTranslating = true;
        cameraRotTime = false;

        Destroy(ballGO, 2f);
        Destroy(rootGO, 2f);
        Destroy(playerGO, 2f);

        StartChallenge();

        ran = false;
    }
    IEnumerator MoveCameraToNewPlace()
    {
        //Translation
        Vector3 dir = Challenges[CurChallengeNumber].CameraPlace.position - mainCamera.position;
        Camera.main.transform.Translate(dir.normalized * cameraSpeed * Time.deltaTime, Space.World);
        //Rotation.
        if (!cameraRotTime)
        {
            float rotDifference = Challenges[CurChallengeNumber - 1].CameraPlace.rotation.eulerAngles.y - Challenges[CurChallengeNumber].CameraPlace.rotation.eulerAngles.y;//-32
            float angleChange = Vector3.Distance(Challenges[CurChallengeNumber].CameraPlace.position, mainCamera.position) / cameraSpeed;//44.45/15 ~3
            cameraRotSpeed = Mathf.Abs((rotDifference / angleChange));//~11.
            cameraRotTime = true;
        }

        Vector3 rotation = Quaternion.RotateTowards(mainCamera.rotation, Challenges[CurChallengeNumber].CameraPlace.rotation, Time.deltaTime * cameraRotSpeed).eulerAngles;
        Camera.main.transform.rotation = Quaternion.Euler(rotation.x, rotation.y, 0f);

        if(Vector3.Distance(mainCamera.position, Challenges[CurChallengeNumber].CameraPlace.position) < 0.5f)
        {
            cameraTranslating = false;
        }

        yield return 0;
    }
    void StartChallenge()
    {
        goaled = false;

        ballGO = (GameObject)Instantiate(ballPrefab,
            Challenges[CurChallengeNumber].BallPlace.position, 
            Challenges[CurChallengeNumber].BallPlace.rotation);

        rootGO = (GameObject)Instantiate(parabolaRootPrefab, 
            Challenges[CurChallengeNumber].BallPlace.position, 
            Challenges[CurChallengeNumber].BallPlace.rotation);

        ballGO.GetComponent<ParabolaController>().ParabolaRoot = rootGO;

        playerGO = (GameObject)Instantiate(player,
           Challenges[CurChallengeNumber].PlayerPlace.position,
           Challenges[CurChallengeNumber].PlayerPlace.rotation);

        Challenges[CurChallengeNumber].Gates.gameObject.GetComponentInChildren<GoalPlace>().Goaled = false;
    }
}
