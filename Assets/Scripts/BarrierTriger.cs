﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierTriger : MonoBehaviour
{
    public Barrier barrier;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            barrier.enabled = false;
        }
    }
}
