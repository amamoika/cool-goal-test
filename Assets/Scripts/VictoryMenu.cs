﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryMenu : MonoBehaviour
{
    public Text levelText;
    public Text rewardText;

    public LevelManager levelManager;
    // Start is called before the first frame update
    void Start()
    {
        levelText.text = levelManager.levelNumber.ToString();
        rewardText.text = levelManager.reward.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
