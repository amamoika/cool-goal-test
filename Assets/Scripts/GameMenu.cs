﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameMenu : MonoBehaviour
{
    public LevelManager levelManager;

    //public Text Cash;
    public GameObject[] levelProgression;

    //public Image[] levelProgImgEmpty;
    public GameObject[] levelProgSmall;
    //public Sprite levelProgImgEmpty;

    public GameObject[] levelProgBig;

    public Text curLevel;
    public Text nextLevel;

    //List<Canvas> levelProgImgs;
    //Image[] a;

    int curChallenge = 0;

    void Start()
    {
        //levelProgImgs = new List<Canvas>(levelManager.Challenges.Count);

        //GameObject levelProgObj1 = Instantiate(levelProgObj, levelProgression[0]);
        //levelProgObj1.GetComponentInChildren<Text>().text = levelManager.levelNumber.ToString();

        //levelProgImgs.Add(Instantiate(levelProgImg, levelProgression.transform).GetComponentInChildren<Image>()[0]);
        //a.CopyTo(levelProgImgs[0].GetComponentsInChildren<Image>(), 0);
        //[1].enabled = true;

        //for (int i = 1; i < levelManager.Challenges.Count; i++)
        //{
        //    levelProgImgs.Add(Instantiate(levelProgImg, levelProgression.transform));
        //}

        //GameObject levelProgObj2 = Instantiate(levelProgObj, levelProgression.transform);
        //levelProgObj2.GetComponentInChildren<Text>().text = (levelManager.levelNumber + 1).ToString();

        curLevel.text = (levelManager.levelNumber).ToString();
        nextLevel.text = (levelManager.levelNumber + 1).ToString();
    }

    public void NextChallenge()
    {
        Fill();
        levelProgSmall[curChallenge].SetActive(true);
    }
    public void Fill()
    {
        levelProgBig[curChallenge].SetActive(true);
        curChallenge++;
    }
}
