﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Transform pointer;

    public ParabolaController parabola;
    public GameObject parabolaRoot;

    public bool Hitted = false;
    public bool Stoped = false;

    public float speed = 0;

    float speedLimit = 0.001f;

    public Treager treager;
    Vector3 lastPosition = Vector3.zero;

    void Start()
    {
        parabola = GetComponent<ParabolaController>();
    }

    void FixedUpdate()
    {
        if (treager.pulsed)
        {
            speed = (transform.position - lastPosition).magnitude;
            lastPosition = transform.position;
            if (speed < speedLimit)
            {
                Stoped = true;
            }
        }

    }

    public void Hit()
    {
        Hitted = true;
        parabola.FollowParabola();
    }
}
